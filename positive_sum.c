#include <stdio.h>
#include <stdbool.h>
int main(){
	int total=0;
	int n;
	while(true){
		printf("Enter: ");
		scanf("%d", &n);
		// take only positive integers
		if (n==0 || n<0){
			break;
		}
		total+=n;
		printf("The sum of the positive numbers entered is: %d \n", total);
	       
	}
	return 0;
}	


		
