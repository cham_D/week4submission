#include <stdio.h>

int main(){
	int N;
	int n;
	int multiple;
	printf("Enter the number: ");
	scanf("%d", &N);
	for (n=1; n<=N; n+=1){
		multiple=N*n;
		printf("%d x %d = %d \n", N, n, multiple);
	}
	return 0;
}
