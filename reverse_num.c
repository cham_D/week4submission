#include <stdio.h>

int main(){
	int N;
	printf("Enter the number: ");
	scanf("%d", &N);
	int rvs_num=0;
	int remainder;
	while (N>0){
		remainder=N%10;
		rvs_num=(rvs_num*10)+remainder;
		//get the quotient
		N=N/10;
	}
	printf("The number in reverse: %d \n", rvs_num);
	return 0;
}

